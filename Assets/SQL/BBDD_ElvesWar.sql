DROP DATABASE IF EXISTS ELVESWAR;
CREATE DATABASE ELVESWAR;
GRANT ALL PRIVILEGES ON ELVESWAR.* TO 'EWAdmin'@'localhost' IDENTIFIED BY 'EWAdmin' WITH GRANT OPTION;
USE ELVESWAR;

CREATE TABLE EMAILS (
	email VARCHAR(50) PRIMARY KEY
) ENGINE=INNODB;
    
CREATE TABLE USERS (
	username		VARCHAR(30) PRIMARY KEY,
    passcode		TEXT NOT NULL,
	email			VARCHAR(50) NOT NULL,
    ingame			BOOLEAN NOT NULL,
    FOREIGN KEY (email) REFERENCES EMAILS (email)
) ENGINE=INNODB;


CREATE TABLE STATS (
	username		VARCHAR(30) PRIMARY KEY,
    kills			INT NOT NULL,
    deaths			INT NOT NULL,
    hits			INT NOT NULL,
    shots			INT NOT NULL,
    wins			INT NOT NULL,
    loses	 		INT NOT NULL,
    FOREIGN KEY (username) REFERENCES USERS (username)
) ENGINE=INNODB;



CREATE TABLE CHARACTERS (
	id 				VARCHAR(30) PRIMARY KEY,
    hp				INT NOT NULL,
    dmg				INT NOT NULL,
    spd				INT NOT NULL
) ENGINE=INNODB;

CREATE TABLE LOBBY (
	username		VARCHAR(30) PRIMARY KEY,
    ip				VARCHAR(50) NOT NULL,
    charId	 		VARCHAR(30) NOT NULL,
    FOREIGN KEY (username) REFERENCES USERS (username)
) ENGINE=INNODB;


INSERT INTO CHARACTERS VALUES("A",40,10,6);
INSERT INTO CHARACTERS VALUES("B",40,10,5);

INSERT INTO EMAILS VALUES ("chevehumor@gmail.com");
INSERT INTO EMAILS VALUES ("user1@gmail.com");
INSERT INTO EMAILS VALUES ("user2@gmail.com");
INSERT INTO USERS VALUES ("Chevitops","1234","chevehumor@gmail.com",true);
INSERT INTO USERS VALUES ("user1","user1","user1@gmail.com",false);
INSERT INTO USERS VALUES ("user2","user2","user2@gmail.com",false);

select * from users;

SELECT * FROM LOBBY WHERE username NOT LIKE "peino";

UPDATE USERS SET ingame = false WHERE username = "bundolo";

select * from users where name = "cholo";
select COUNT(*) from users;
#UPDATE USERS SET ingame = false WHERE username = "Chevitops";
#SELECT * FROM STATS ORDER BY kills,wins DESC LIMIT 10;

#UPDATE STATS SET kills=@kills, deaths=@deaths, hits=@hits, shots=@shots, wins=@wins, loses=@loses WHERE username=@username;