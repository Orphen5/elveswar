﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MusicInitial : MonoBehaviour {

    public static MusicInitial instance;
    void Awake () {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName.Equals("firstUi")
          || sceneName.Equals("mainMenu") 
          || sceneName.Equals("RegisterUi")
          || sceneName.Equals("worldRanking"))
        {
            if (instance == null) instance = this;
            else if (instance != this) Destroy(gameObject);
            DontDestroyOnLoad(gameObject);
        }
    }

    void Update()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (!sceneName.Equals("firstUi")
            & !sceneName.Equals("mainMenu")
            & !sceneName.Equals("RegisterUi")
            & !sceneName.Equals("worldRanking"))
        {
            DestroyImmediate(gameObject);
        }
    }
}
