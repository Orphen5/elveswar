﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicSelect : MonoBehaviour {
    public static MusicSelect instance;
    void Awake()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (sceneName.Equals("preparingGame")
          || sceneName.Equals("SelectCharUi")
          || sceneName.Equals("startingGame"))
        {
            if (instance == null) instance = this;
            else if (instance != this) Destroy(gameObject);
            DontDestroyOnLoad(gameObject);
        }
    }

    void Update()
    {
        string sceneName = SceneManager.GetActiveScene().name;
        if (!sceneName.Equals("preparingGame")
            & !sceneName.Equals("SelectCharUi")
            & !sceneName.Equals("startingGame"))
        {
            DestroyImmediate(gameObject);
        }
    }
}
