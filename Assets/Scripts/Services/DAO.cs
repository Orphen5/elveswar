﻿using Assets.Scripts;
using Assets.Scripts.Beans;
using MySql.Data.MySqlClient;
using System.Collections;
using UnityEngine;

public class DAO : MonoBehaviour
{

    public static int getLogin(string user, string pass)
    {
        int option = 87;
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        pass = Utility.EncodePassword(pass);
        command.CommandText = "SELECT ingame FROM USERS WHERE username = @name AND passcode = @pass";
        command.Parameters.AddWithValue("@name", user);
        command.Parameters.AddWithValue("@pass", pass);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        if (Reader.Read())
        {
            option = ((Reader.GetInt32("ingame")) == 0) ? 1 : 2;
        }
        else
        {
            option = 3;
        }
        con.Close();
        return option;
    }

    public static int userExists(string user)
    {
        int option = 87;
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT * FROM USERS WHERE username = @name";
        command.Parameters.AddWithValue("@name", user);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        if (Reader.Read())
        {
            option = 1;
        }
        else
        {
            option = 0;
        }
        con.Close();
        return option;
    }

    public static int mailExists(string email)
    {
        int option = 87;
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT * FROM EMAILS WHERE email = @email";
        command.Parameters.AddWithValue("@email", email);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        if (Reader.Read())
        {
            option = 1;
        }
        else
        {
            option = 0;
        }
        con.Close();
        return option;
    }

    public static void registerUser(string user, string pass, string email)
    {
        pass = Utility.EncodePassword(pass);

        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "INSERT INTO EMAILS VALUES (@email)";
        command.Parameters.AddWithValue("@email", email);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();

        con = null;
        command = null;

        con = DatabaseHandler.getCon();
        command = con.CreateCommand();
        command.CommandText = "INSERT INTO USERS VALUES(@user, @pass, @email, false)";
        command.Parameters.AddWithValue("@user", user);
        command.Parameters.AddWithValue("@pass", pass);
        command.Parameters.AddWithValue("@email", email);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static void userConnected(string user)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "UPDATE USERS SET ingame = true WHERE username = @name";
        command.Parameters.AddWithValue("@name", user);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static void userDisconnected(string user)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "UPDATE USERS SET ingame = false WHERE username = @name";
        command.Parameters.AddWithValue("@name", user);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static ArrayList worldRanking(int limit)
    {
        ArrayList stats = new ArrayList();
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT * FROM STATS ORDER BY kills,wins DESC LIMIT @limit";
        command.Parameters.AddWithValue("@limit", limit);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        string username;
        int kills;
        int deaths;
        int hits;
        int shots;
        int wins;
        int loses;
        while (Reader.Read())
        {
            username = Reader.GetString("username");
            kills = Reader.GetInt32("kills");
            deaths = Reader.GetInt32("deaths");
            hits = Reader.GetInt32("hits");
            shots = Reader.GetInt32("shots");
            wins = Reader.GetInt32("wins");
            loses = Reader.GetInt32("loses");
            stats.Add(new UserStats(username, kills, deaths, hits, shots, wins, loses));
        }
        con.Close();
        return stats;
    }

    public static UserStats getUserStats(string name)
    {
        UserStats stats = new UserStats(name);
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT kills,deaths,hits,shots,wins,loses FROM STATS WHERE username = @name";
        command.Parameters.AddWithValue("@name", name);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        int kills;
        int deaths;
        int hits;
        int shots;
        int wins;
        int loses;
        while (Reader.Read())
        {
            kills = Reader.GetInt32("kills");
            deaths = Reader.GetInt32("deaths");
            hits = Reader.GetInt32("hits");
            shots = Reader.GetInt32("shots");
            wins = Reader.GetInt32("wins");
            loses = Reader.GetInt32("loses");
            stats = new UserStats(name, kills, deaths, hits, shots, wins, loses);
        }
        con.Close();
        return stats;
    }

    public static void setPlayerStatsBBDD(UserStats stats)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "UPDATE STATS SET kills = @kills, deaths = @deaths, hits = @hits, shots = @shots, wins = @wins, loses = @loses WHERE username = @username";
        command.Parameters.AddWithValue("@kills", stats.Kills);
        command.Parameters.AddWithValue("@deaths", stats.Deaths);
        command.Parameters.AddWithValue("@hits", stats.Hits);
        command.Parameters.AddWithValue("@shots", stats.Shots);
        command.Parameters.AddWithValue("@wins", stats.Wins);
        command.Parameters.AddWithValue("@loses", stats.Loses);
        command.Parameters.AddWithValue("@username", stats.Username);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static CharStats getCharStats(string id)
    {
        CharStats charStats = new CharStats();
        charStats.Id = id;
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT hp, dmg, spd FROM CHARACTERS WHERE id = @id";
        command.Parameters.AddWithValue("@id", id);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        int hp;
        int dmg;
        int spd;
        while (Reader.Read())
        {
            hp = Reader.GetInt32("hp");
            dmg = Reader.GetInt32("dmg");
            spd = Reader.GetInt32("spd");
            charStats.Hp = hp;
            charStats.Damage = dmg;
            charStats.Speed = spd;


        }
        con.Close();
        return charStats;
    }

    /* SELECCION DE PERSONAJE */

    public static bool getLobbyUsers(string user)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT * FROM LOBBY WHERE username NOT LIKE @user";
        command.Parameters.AddWithValue("@user", user);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        if (Reader.Read())
        {
            con.Close();
            return true;
        }
        else
        {
            con.Close();
            return false;
        }
    }

    public static bool waitLobbyToSelectCharacter(string user)
    {
        bool flag = false;
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT charId FROM LOBBY WHERE username NOT LIKE @user";
        command.Parameters.AddWithValue("@user", user);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if (Reader.GetString("charId").Equals("Preparing"))
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        con.Close();
        return flag;
    }

    public static void registerLobbly(string user, string ip)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "INSERT INTO LOBBY VALUES (@username, @ip, @charId)";
        command.Parameters.AddWithValue("@username", user);
        command.Parameters.AddWithValue("@ip", ip);
        command.Parameters.AddWithValue("@charId", "Preparing");
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static void registerCharInLobbly(string user, string charId)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        Debug.Log("User: " + user + "  CharID: " + charId);
        command.CommandText = "UPDATE LOBBY SET charId = @charId WHERE username = @username";
        command.Parameters.AddWithValue("@charId", charId);
        command.Parameters.AddWithValue("@username", user);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static bool startGame()
    {
        bool flag = false;
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT COUNT(*) FROM LOBBY WHERE charId NOT LIKE @Preparing";
        command.Parameters.AddWithValue("@Preparing", "Preparing");
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            if (Reader.GetInt64("COUNT(*)") == (2))
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
        }
        con.Close();
        return flag;
    }

    public static string getIpVersus(string user)
    {
        string ip = "";
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT ip FROM LOBBY WHERE username NOT LIKE @user";
        command.Parameters.AddWithValue("@user", user);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        if (Reader.Read())
        {
           ip = Reader.GetString("ip");
        }
        con.Close();
        return ip;
    }

    public static string getCharSelected(string user)
    {
        string charSelected = "";
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "SELECT charId FROM LOBBY WHERE username NOT LIKE @user";
        command.Parameters.AddWithValue("@user", user);
        MySqlDataReader Reader;
        con.Open();
        Reader = command.ExecuteReader();
        while (Reader.Read())
        {
            charSelected = Reader.GetString("charId");
        }
        con.Close();
        return charSelected;
    }

    public static void deleteLobby(string user)
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "DELETE FROM LOBBY WHERE username = @username";
        command.Parameters.AddWithValue("@username", user);
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }

    public static void cleanLobby()
    {
        MySqlConnection con = DatabaseHandler.getCon();
        MySqlCommand command = con.CreateCommand();
        command.CommandText = "DELETE FROM LOBBY";
        con.Open();
        command.ExecuteNonQuery();
        con.Close();
    }
}