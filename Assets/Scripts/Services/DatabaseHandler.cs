﻿using UnityEngine;
using System;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;

public class DatabaseHandler : MonoBehaviour
{
    private static string host = "sql8.freemysqlhosting.net";
    private static string database = "sql8175046";
    private static string user = "sql8175046";
    private static string password = "7d9biPZqDp";
    private static bool pooling = true;

    private static string connectionString;
    private static MySqlConnection con = null;

    public static DatabaseHandler instance;

    void Awake()
    {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        connectionString = "Server=" + host + ";Database=" + database + ";User=" + user + ";Password=" + password + ";Pooling=";
        if (pooling)
        {
            connectionString += "true;";
        }
        else
        {
            connectionString += "false;";
        }

        try
        {
            con = new MySqlConnection(connectionString);
            Debug.Log("Mysql State: " + con.State);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
    }

    void OnApplicationQuit()
    {
        if (con != null)
        {
            if (con.State.ToString() != "Closed")
            {
                con.Close();
                Debug.Log("MySQL Connection closed");
            }
            con.Dispose();
        }
    }

    public string GetConnectionState()
    {
        return con.State.ToString();
    }

    public static MySqlConnection getCon()
    {
        return con;
    }
}
