﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectaSuelo : MonoBehaviour {

	void OnCollisionEnter(Collision coll) {

        if (coll.transform.tag.Equals("Suelo")) {
            GetComponent<PlayerManager>().allowJump = true;
        }
    }

    void OnCollisionExit(Collision coll) {

        if (coll.transform.tag.Equals("Suelo")) {
            GetComponent<PlayerManager>().allowJump = false;
        }
    }
}
