﻿using Assets.Scripts.Beans;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GameManager : NetworkBehaviour {

    //Variable para controlar el usuaro
    public static UserBean user = new UserBean();

    //Esta variable nos permite comunicarnos entre scripts usando atributos y funciones públicas de la clase GameManager, accediendo a través de GameManager.instance.
    public static GameManager instance;

    //Definimos si somos player A o B
    [HideInInspector]
    public string tipoPlayer;

    //Definimos los distintos estados de la partida
    public enum EstadoPartida {
        SeleccionandoJugador,
        InGame,
        FinPartida };

    [HideInInspector]
    public EstadoPartida estadoPartida;

    //Creamos un array de players
    [HideInInspector]
    public PlayerManager[] players;

    bool playerInstanciado;
    bool desactivadoRival;

    [HideInInspector]
    public Vector3 posInicialA;
    [HideInInspector]
    public Vector3 posInicialB;

    void Awake() {
        if (instance == null) instance = this;
        else if (instance != this) Destroy(gameObject);
        DontDestroyOnLoad(gameObject);

        posInicialA = new Vector3(33.57f, 0.0f, 29.45f);
        posInicialB = new Vector3(-35.0f, 0.0f, -30.0f);
        playerInstanciado = false;
        desactivadoRival = false;
        estadoPartida = EstadoPartida.SeleccionandoJugador;
    }

    void Update() {
        //Debug.Log(estadoPartida);
        switch (estadoPartida) {
            case EstadoPartida.SeleccionandoJugador:
                if (isServer && !playerInstanciado) {
                    playerInstanciado = true;
                    InstanciaPlayer.instance.CmdInstanciarPlayer(posInicialB);
                }

                cargaPlayers("elfaNocheHunter(Clone)", "elfoSangreHunter(Clone)");
                break;

            case EstadoPartida.InGame:
                DAO.deleteLobby(user.UserStats.Username);
                tipoPlayer = user.CharStats.Id;
                //Recorremos el array de players que tenemos en escena y permitimos tomar el control del que se corresponda con mi tipoPlayer
                for (int i = 0; i < players.Length; i++) {
                    if (players[i].tag.Equals(tipoPlayer)) {
                        players[i].GetComponent<PlayerController>().capturaInputs();
                        players[i].GetComponent<RotaCamara>().capturaRaton();
                        StartCoroutine(players[i].maquinaEstados());
                        players[i].GetComponent<CanvasManager>().actualizaPuntosPropios(players[i].GetComponent<PlayerManager>().puntos);
                        players[i].GetComponent<CanvasManager>().actualizaDisparosPropios(players[i].GetComponent<PlayerManager>().disparos);
                        players[i].GetComponent<CanvasManager>().actualizaAciertosPropios(players[i].GetComponent<PlayerManager>().aciertos);
                        players[i].GetComponent<CanvasManager>().actualizaHeadshotsPropios(players[i].GetComponent<PlayerManager>().headshots);
                        players[i].GetComponent<CanvasManager>().actualizaAccuracyPropios(players[i].GetComponent<PlayerManager>().disparos, players[i].GetComponent<PlayerManager>().aciertos);

                        if (i == 0) {
                            players[i].GetComponent<CanvasManager>().actualizaPuntosRival(players[1].GetComponent<PlayerManager>().puntos);
                            players[i].GetComponent<CanvasManager>().actualizaDisparosRival(players[1].GetComponent<PlayerManager>().disparos);
                            players[i].GetComponent<CanvasManager>().actualizaAciertosRival(players[1].GetComponent<PlayerManager>().aciertos);
                            players[i].GetComponent<CanvasManager>().actualizaHeadshotsRival(players[1].GetComponent<PlayerManager>().headshots);
                            players[i].GetComponent<CanvasManager>().actualizaAccuracyRival(players[1].GetComponent<PlayerManager>().disparos, players[1].GetComponent<PlayerManager>().aciertos);
                        }
                        else if (i == 1) {
                            players[i].GetComponent<CanvasManager>().actualizaPuntosRival(players[0].GetComponent<PlayerManager>().puntos);
                            players[i].GetComponent<CanvasManager>().actualizaDisparosRival(players[0].GetComponent<PlayerManager>().disparos);
                            players[i].GetComponent<CanvasManager>().actualizaAciertosRival(players[0].GetComponent<PlayerManager>().aciertos);
                            players[i].GetComponent<CanvasManager>().actualizaHeadshotsRival(players[0].GetComponent<PlayerManager>().headshots);
                            players[i].GetComponent<CanvasManager>().actualizaAccuracyRival(players[0].GetComponent<PlayerManager>().disparos, players[0].GetComponent<PlayerManager>().aciertos);
                        }

                        Debug.Log("Vida propia: " + players[i].vida);
                        Debug.Log("Puntos propios: " + players[i].puntos);
                    }
                    else {
                        if (!desactivadoRival) {
                            desactivadoRival = true;
                            players[i].transform.FindChild("Camera").GetComponent<Camera>().enabled = false;
                            players[i].transform.FindChild("Camera").GetComponent<AudioListener>().enabled = false;
                            players[i].transform.FindChild("Camera").transform.FindChild("Canvas").gameObject.SetActive(false);
                            players[i].transform.FindChild("Camera").transform.FindChild("damageHUD").gameObject.SetActive(false);
                            players[i].transform.FindChild("Camera").transform.FindChild("CanvasExit").gameObject.SetActive(false);
                            players[i].transform.FindChild("Camera").transform.FindChild("Puntero").gameObject.SetActive(false);
                        }

                        Debug.Log("Vida rival: " + players[i].vida);
                        Debug.Log("Puntos rival: " + players[i].puntos);
                    }

                    //Ejecutamos la maquina de estados para todos los players de la partida
                    StartCoroutine(players[i].maquinaEstadosGlobal(!players[i].tag.Equals(tipoPlayer)));
                }
                break;

            case EstadoPartida.FinPartida:
                break;
        }
    }

    void cargaPlayers(string player1, string player2) {

        //Ponemos a los characters como hijos de "Players"
        if (GameObject.Find(player1) != null && GameObject.Find(player2) != null) {
            GameObject.Find(player1).transform.parent = GameObject.Find("Players").transform;
            GameObject.Find(player2).transform.parent = GameObject.Find("Players").transform;

            //Recorre el gameObject "Players" buscando hijos y los añade al array de players
            players = GameObject.Find("Players").GetComponentsInChildren<PlayerManager>();

            //Escondemos el ratón
            Cursor.visible = false;

            estadoPartida = EstadoPartida.InGame;
        }
    }
}