﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorManager : MonoBehaviour {

    Animator anim;

    void Awake() {
        //Asociamos al objeto anim el componente animator del modelo
        anim = GetComponent<Animator>();
    }

    public void animIdle() { anim.SetInteger("estado", 0); }
    public void animCorrer() { anim.SetInteger("estado", 1); }
    public void animAndar() { anim.SetInteger("estado", 2); }
    public void animSalto() { anim.SetInteger("estado", 3); }
    public void animDisparo() { anim.SetInteger("estado", 4); }
    public void animCritical() { anim.SetInteger("estado", 5); }
    public void animMuerte() { anim.SetInteger("estado", 6); }
}
