﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour {

    GameObject canvas;
    GameObject canvasExit;
    Color transparencia;
    Color normal;

    void Awake() {
        canvas = transform.FindChild("Camera").transform.FindChild("Canvas").gameObject;
        canvasExit = transform.FindChild("Camera").transform.FindChild("CanvasExit").gameObject;
        transparencia = new Color(1, 1, 1, 0.3f);
        normal = new Color(1, 1, 1, 1);
    }

    public void sumaPuntos(int puntos) {
        canvas.transform.FindChild("Puntos").GetComponent<Text>().text = puntos.ToString();
    }

    public void transparenciaVidas(int vida) {

        switch (vida) {
            case 0:
                canvas.transform.FindChild("PuntosSalud").FindChild("salud1").GetComponent<SpriteRenderer>().color = transparencia;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud2").GetComponent<SpriteRenderer>().color = transparencia;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud3").GetComponent<SpriteRenderer>().color = transparencia;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud4").GetComponent<SpriteRenderer>().color = transparencia;
                break;

            case 10:
                canvas.transform.FindChild("PuntosSalud").FindChild("salud1").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud2").GetComponent<SpriteRenderer>().color = transparencia;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud3").GetComponent<SpriteRenderer>().color = transparencia;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud4").GetComponent<SpriteRenderer>().color = transparencia;
                break;

            case 20:
                canvas.transform.FindChild("PuntosSalud").FindChild("salud1").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud2").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud3").GetComponent<SpriteRenderer>().color = transparencia;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud4").GetComponent<SpriteRenderer>().color = transparencia;
                break;

            case 30:
                canvas.transform.FindChild("PuntosSalud").FindChild("salud1").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud2").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud3").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud4").GetComponent<SpriteRenderer>().color = transparencia;
                break;

            case 40:
                canvas.transform.FindChild("PuntosSalud").FindChild("salud1").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud2").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud3").GetComponent<SpriteRenderer>().color = normal;
                canvas.transform.FindChild("PuntosSalud").FindChild("salud4").GetComponent<SpriteRenderer>().color = normal;
                break;
        }
    }

    public IEnumerator activaDamageHUD(float tiempo) {
        transform.FindChild("Camera").transform.FindChild("damageHUD").gameObject.SetActive(true);
        yield return new WaitForSeconds(tiempo);
        transform.FindChild("Camera").transform.FindChild("damageHUD").gameObject.SetActive(false);
    }

    public void activaCanvasExit() {
        canvasExit.SetActive(!canvasExit.activeSelf);

        if (canvasExit.activeSelf)
        {
            Cursor.visible = true;
            canvas.SetActive(false);
        }
        else if (!canvasExit.activeSelf)
        {
            Cursor.visible = false;
            canvas.SetActive(true);
        }
    }

    public void resumeGame() {        
        canvas.SetActive(true);
        canvasExit.SetActive(false);
        Cursor.visible = false;
    }

    public void goMainMenu() {
        SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
    }

    public void exitGame() {
        DAO.userDisconnected(GameManager.user.UserStats.Username);
        Application.Quit();
    }

    public void actualizaPuntosPropios(int puntos) {
        canvasExit.transform.FindChild("Puntos").GetComponent<Text>().text = puntos.ToString();
    }

    public void actualizaPuntosRival(int puntos) {
        canvasExit.transform.FindChild("PuntosRival").GetComponent<Text>().text = puntos.ToString();
    }

    public void actualizaDisparosPropios(int disparos) {
        canvasExit.transform.FindChild("Disparos").GetComponent<Text>().text = disparos.ToString();
    }

    public void actualizaDisparosRival(int disparos) {
        canvasExit.transform.FindChild("DisparosRival").GetComponent<Text>().text = disparos.ToString();
    }

    public void actualizaAciertosPropios(int aciertos) {
        canvasExit.transform.FindChild("Aciertos").GetComponent<Text>().text = aciertos.ToString();
    }

    public void actualizaAciertosRival(int aciertos) {
        canvasExit.transform.FindChild("AciertosRival").GetComponent<Text>().text = aciertos.ToString();
    }

    public void actualizaHeadshotsPropios(int headshots) {
        canvasExit.transform.FindChild("Headshots").GetComponent<Text>().text = headshots.ToString();
    }

    public void actualizaHeadshotsRival(int headshots) {
        canvasExit.transform.FindChild("HeadshotsRival").GetComponent<Text>().text = headshots.ToString();
    }

    public void actualizaAccuracyPropios(int disparos, int aciertos) {
        if (disparos != 0)
            canvasExit.transform.FindChild("Accuracy").GetComponent<Text>().text = ((aciertos * 100) / disparos).ToString() + "%";
    }

    public void actualizaAccuracyRival(int disparos, int aciertos) {
        if (disparos != 0)
            canvasExit.transform.FindChild("AccuracyRival").GetComponent<Text>().text = ((aciertos * 100) / disparos).ToString() + "%";
    }
}