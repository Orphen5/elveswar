﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerManager : NetworkBehaviour {

    //Definimos los estados que puede tener un jugador
    public enum EstadosPlayer {
        Idle,
        Andando,
        LateralI,
        LateralD,
        Corriendo,
        DiagonalDrchaCorriendo,
        DiagonalIzqCorriendo,
        DiagonalDrchaAndando,
        DiagonalIzqAndando,
        Saltando,
        SaltandoAdelante,
        SaltandoAtras,
        SaltandoIzq,
        SaltandoDrcha,
        Disparando,
        RecibiendoDaño,
        Muriendo };

    [HideInInspector]
    public EstadosPlayer estadoPlayer;
    [HideInInspector]
    public bool allowJump;

    [HideInInspector]
    [SyncVar]
    public int vida, puntos, disparos, aciertos, headshots;

    int vidaActual;
    int puntosActuales;

    void Start() {
        estadoPlayer = EstadosPlayer.Idle;
        allowJump = true;
        vida = GetComponent<PlayerStats>().vida;
        puntos = 0;
        disparos = 0;
        aciertos = 0;
        headshots = 0;
        vidaActual = vida;
        puntosActuales = puntos;
    }

    public IEnumerator maquinaEstados() {
        //Debug.Log(gameObject.name + " - " + estadoPlayer);
        switch (estadoPlayer) {
            case EstadosPlayer.Idle:
                GetComponent<AnimatorManager>().animIdle();
                break;

            case EstadosPlayer.Andando:
                GetComponent<AnimatorManager>().animAndar();
                andar();
                break;

            case EstadosPlayer.LateralI:
                GetComponent<AnimatorManager>().animAndar();
                lateralIzq();
                break;

            case EstadosPlayer.LateralD:
                GetComponent<AnimatorManager>().animAndar();
                lateralDrcha();
                break;

            case EstadosPlayer.Corriendo:
                GetComponent<AnimatorManager>().animCorrer();
                correr();
                break;

            case EstadosPlayer.DiagonalDrchaCorriendo:
                GetComponent<AnimatorManager>().animCorrer();
                diagonalDrchaCorriendo();
                break;

            case EstadosPlayer.DiagonalIzqCorriendo:
                GetComponent<AnimatorManager>().animCorrer();
                diagonalIzqCorriendo();
                break;

            case EstadosPlayer.DiagonalDrchaAndando:
                GetComponent<AnimatorManager>().animAndar();
                diagonalDrchaAndando();
                break;

            case EstadosPlayer.DiagonalIzqAndando:
                GetComponent<AnimatorManager>().animAndar();
                diagonalIzqAndando();
                break;

            case EstadosPlayer.Saltando:
                GetComponent<AnimatorManager>().animSalto();
                saltar();
                break;

            case EstadosPlayer.SaltandoAdelante:
                GetComponent<AnimatorManager>().animSalto();
                saltarAdelante();
                break;

            case EstadosPlayer.SaltandoAtras:
                GetComponent<AnimatorManager>().animSalto();
                saltarAtras();
                break;

            case EstadosPlayer.SaltandoIzq:
                GetComponent<AnimatorManager>().animSalto();
                saltarIzq();
                break;

            case EstadosPlayer.SaltandoDrcha:
                GetComponent<AnimatorManager>().animSalto();
                saltarDrcha();
                break;

            case EstadosPlayer.Disparando:
                GetComponent<AnimatorManager>().animDisparo();
                if (!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(GetComponent<NombresAnimaciones>().anim[0]) && 
                    !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(GetComponent<NombresAnimaciones>().anim[1]) &&
                    !GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(GetComponent<NombresAnimaciones>().anim[2]))
                {
                    yield return new WaitForSeconds(0.5f);
                    GetComponent<InstanciaFlecha>().CmdInstanciarFlecha();
                }
                break;
        }
    }

    public IEnumerator maquinaEstadosGlobal(bool playerRival) {

        if (vidaActual != vida) {

            if (vida > 0) {
                estadoPlayer = EstadosPlayer.RecibiendoDaño;
            }
            else {
                estadoPlayer = EstadosPlayer.Muriendo;
            }

            GetComponent<CanvasManager>().transparenciaVidas(vida);
            vidaActual = vida;
        }

        if (puntosActuales != puntos) {
            GetComponent<CanvasManager>().sumaPuntos(puntos);
            puntosActuales = puntos;
        }

        switch (estadoPlayer) {
            case EstadosPlayer.RecibiendoDaño:
                if(!playerRival) StartCoroutine(GetComponent<CanvasManager>().activaDamageHUD(2.0f));
                GetComponent<AnimatorManager>().animCritical();
                if (!GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(GetComponent<NombresAnimaciones>().anim[2])) {
                    estadoPlayer = EstadosPlayer.Idle;
                }
                break;

            case EstadosPlayer.Muriendo:
                if (!playerRival) StartCoroutine(GetComponent<CanvasManager>().activaDamageHUD(7.0f));
                GetComponent<AnimatorManager>().animMuerte();
                yield return new WaitForSeconds(7.0f);
                reSpawn();
                break;
        }
    }

    void correr() {
        transform.Translate(Vector3.left * GetComponent<PlayerStats>().velocidadCorrer * Time.deltaTime);
    }

    void diagonalDrchaCorriendo() {
        transform.Translate(new Vector3(-1, 0, 1) * GetComponent<PlayerStats>().velocidadCorrer * Time.deltaTime);
    }

    void diagonalIzqCorriendo() {
        transform.Translate(new Vector3(-1, 0, -1) * GetComponent<PlayerStats>().velocidadCorrer * Time.deltaTime);
    }

    void andar() {
        transform.Translate(Vector3.right * GetComponent<PlayerStats>().velocidadAndar * Time.deltaTime);
    }

    void diagonalDrchaAndando() {
        transform.Translate(new Vector3(1, 0, 1) * GetComponent<PlayerStats>().velocidadAndar * Time.deltaTime);
    }

    void diagonalIzqAndando() {
        transform.Translate(new Vector3(1, 0, -1) * GetComponent<PlayerStats>().velocidadAndar * Time.deltaTime);
    }

    void lateralIzq() {
        transform.Translate(Vector3.back * GetComponent<PlayerStats>().velocidadAndar * Time.deltaTime);
    }

    void lateralDrcha() {
        transform.Translate(Vector3.forward * GetComponent<PlayerStats>().velocidadAndar * Time.deltaTime);
    }

    void saltar() {
        GetComponent<Rigidbody>().AddRelativeForce(Vector3.up * GetComponent<PlayerStats>().fuerzaSalto, ForceMode.Impulse);
    }

    void saltarAdelante() {
        GetComponent<Rigidbody>().AddRelativeForce(new Vector3(-1.0f, 1.0f, 0) * GetComponent<PlayerStats>().fuerzaSaltoAdelante, ForceMode.Impulse);
    }

    void saltarAtras() {
        GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0.6f, 1.4f, 0) * GetComponent<PlayerStats>().fuerzaSaltoAtras, ForceMode.Impulse);
    }

    void saltarIzq() {
        GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 1.2f, -1.0f) * GetComponent<PlayerStats>().fuerzaSaltoAtras, ForceMode.Impulse);
    }

    void saltarDrcha() {
        GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 1.2f, 1.0f) * GetComponent<PlayerStats>().fuerzaSaltoAtras, ForceMode.Impulse);
    }

    void reSpawn() {

        if (gameObject.tag.Equals("A")) {
            transform.position = GameManager.instance.posInicialA;
        }
        else {
            transform.position = GameManager.instance.posInicialB;
        }

        vida = GetComponent<PlayerStats>().vida;
        GetComponent<CanvasManager>().transparenciaVidas(vida);
        estadoPlayer = EstadosPlayer.Idle;
    }
}
