﻿using UnityEngine;
using UnityEngine.Networking;

public class MyNetworkManager : MonoBehaviour {

    bool isAtStartup;
    string direccionIP;
    string tipoPlayer;
    const int puerto = 4444;

    void Awake() {
        isAtStartup = true;
        direccionIP = GameManager.user.IpRival;
        tipoPlayer = GameManager.user.CharStats.Id;
    }

    void asignaTipoPlayer() {
        switch (tipoPlayer)
        {
            case "A":
                GetComponent<NetworkManager>().autoCreatePlayer = true;
                SetupClient();
                break;

            case "B":
                GetComponent<NetworkManager>().autoCreatePlayer = false;
                SetupServer();
                break;
        }              
    }

    void Update() {

        if (isAtStartup) {
            asignaTipoPlayer();
            Debug.Log("Ip adversario: " + direccionIP + "\n tipoPlayer: " + tipoPlayer);
        }
    }

    // Create a server and listen on a port
    public void SetupServer() {
        GetComponent<NetworkManager>().networkPort = puerto;
        GetComponent<NetworkManager>().StartHost();
        isAtStartup = false;
    }

    // Create a client and connect to the server port
    public void SetupClient() {
        GetComponent<NetworkManager>().networkPort = puerto;
        GetComponent<NetworkManager>().networkAddress = direccionIP;
        GetComponent<NetworkManager>().StartClient();
        isAtStartup = false;
    }
}