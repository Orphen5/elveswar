﻿using UnityEngine;

public class PlayerStats : MonoBehaviour {

    [HideInInspector]
    public int vida, damage;

    [HideInInspector]
    public float velocidadCorrer, velocidadAndar, fuerzaSalto, fuerzaSaltoAdelante, fuerzaSaltoAtras, fuerzaDisparo;

    void Awake() {
        
        vida = GameManager.user.CharStats.Hp;
        damage = GameManager.user.CharStats.Damage;
        velocidadCorrer = GameManager.user.CharStats.Speed;

        velocidadAndar = velocidadCorrer / 2;
        fuerzaSalto = velocidadCorrer * 10;
        fuerzaSaltoAdelante = fuerzaSalto - 10;
        fuerzaSaltoAtras = fuerzaSalto - 20;
        fuerzaDisparo = 50.0f;
    }
}
