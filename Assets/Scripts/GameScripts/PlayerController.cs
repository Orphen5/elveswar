﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //Este método detecta los Inputs del usuario, determinarán el movimiento de su personaje
    public void capturaInputs() {

        if (GetComponent<PlayerManager>().estadoPlayer != PlayerManager.EstadosPlayer.Muriendo && GetComponent<PlayerManager>().estadoPlayer != PlayerManager.EstadosPlayer.RecibiendoDaño) {

            if (Input.GetKey(KeyCode.W) && GetComponent<PlayerManager>().allowJump) {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Corriendo;

                if (Input.GetKey(KeyCode.D)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.DiagonalDrchaCorriendo;
                }

                if (Input.GetKey(KeyCode.A)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.DiagonalIzqCorriendo;
                }

                if (Input.GetKeyDown(KeyCode.Space)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.SaltandoAdelante;
                }

                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Disparando;
                }
            }
            else if (Input.GetKey(KeyCode.S) && GetComponent<PlayerManager>().allowJump) {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Andando;

                if (Input.GetKey(KeyCode.D)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.DiagonalDrchaAndando;
                }

                if (Input.GetKey(KeyCode.A)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.DiagonalIzqAndando;
                }

                if (Input.GetKeyDown(KeyCode.Space)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.SaltandoAtras;
                }

                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Disparando;
                }
            }
            else if (Input.GetKey(KeyCode.A) && GetComponent<PlayerManager>().allowJump) {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.LateralI;

                if (Input.GetKeyDown(KeyCode.Space)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.SaltandoIzq;
                }

                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Disparando;
                }
            }
            else if (Input.GetKey(KeyCode.D) && GetComponent<PlayerManager>().allowJump) {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.LateralD;

                if (Input.GetKeyDown(KeyCode.Space)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.SaltandoDrcha;
                }

                if (Input.GetMouseButtonDown(0)) {
                    GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Disparando;
                }
            }
            else if (Input.GetKeyDown(KeyCode.Space) && GetComponent<PlayerManager>().allowJump) {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Saltando;
            }
            else if (Input.GetMouseButtonDown(0)) {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Disparando;
            }
            else {
                GetComponent<PlayerManager>().estadoPlayer = PlayerManager.EstadosPlayer.Idle;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape)) {
            GetComponent<CanvasManager>().activaCanvasExit();
        }
    }
}