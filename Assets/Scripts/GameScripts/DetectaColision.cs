﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DetectaColision : NetworkBehaviour {

    bool impacto;

    void Awake() {
        impacto = false;
    }

    void Update() {
        if (isServer) StartCoroutine(destruirFlecha());
    }

    void OnCollisionEnter(Collision coll) {

        if (!impacto) {
            impacto = true;

            if (isServer) {

                if (coll.transform.tag.Equals("A") || coll.transform.tag.Equals("B")) {

                    foreach (Transform child in GameObject.Find("Players").transform) {
                        
                        if (!child.tag.Equals(coll.transform.tag)) {

                            if (coll.collider.GetType() == typeof(SphereCollider)) {
                                coll.transform.GetComponent<PlayerManager>().vida = 0;
                                child.GetComponent<PlayerManager>().puntos += 30;
                                child.GetComponent<PlayerManager>().aciertos++;
                                child.GetComponent<PlayerManager>().headshots++;
                            }
                            else {
                                coll.transform.GetComponent<PlayerManager>().vida -= child.GetComponent<PlayerStats>().damage;
                                child.GetComponent<PlayerManager>().puntos += 10;
                                child.GetComponent<PlayerManager>().aciertos++;
                            }
                        }
                    }
                }
            }
        }
    }

    IEnumerator destruirFlecha() {
        yield return new WaitForSeconds(10.0f);
        NetworkServer.Destroy(gameObject);
    }
}