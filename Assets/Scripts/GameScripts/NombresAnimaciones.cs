﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NombresAnimaciones : MonoBehaviour {

    string[,] nombreAnimaciones = new string[4, 3] {
        { "LoadBow [38]", "AttackBow [70]", "CombatCritical [21]" }, 
        { "LoadBow [16]", "AttackBow [8]", "CombatCritical [59]" }, 
        { "LoadBow [46]", "AttackBow [67]", "CombatCritical [13]" }, 
        { "LoadBow [33]", "AttackBow [34]", "CombatCritical [57]" }
    };

    [HideInInspector]
    public string[] anim;

    void Awake() {
        anim = new string[3];
        asignaNombres();
    }

    void asignaNombres() {
        switch (gameObject.name) {
            case "elfaNocheHunter(Clone)":
                anim[0] = nombreAnimaciones[0, 0];
                anim[1] = nombreAnimaciones[0, 1];
                anim[2] = nombreAnimaciones[0, 2];
                break;

            case "elfaSangreHunter(Clone)":
                anim[0] = nombreAnimaciones[1, 0];
                anim[1] = nombreAnimaciones[1, 1];
                anim[2] = nombreAnimaciones[1, 2];
                break;

            case "elfoNocheHunter(Clone)":
                anim[0] = nombreAnimaciones[2, 0];
                anim[1] = nombreAnimaciones[2, 1];
                anim[2] = nombreAnimaciones[2, 2];
                break;

            case "elfoSangreHunter(Clone)":
                anim[0] = nombreAnimaciones[3, 0];
                anim[1] = nombreAnimaciones[3, 1];
                anim[2] = nombreAnimaciones[3, 2];
                break;
        }
    }
}
