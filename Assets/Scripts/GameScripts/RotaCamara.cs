﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotaCamara : MonoBehaviour {

    const float correccion = 90.0f;
    const float velocidadX = 5.0f;
    const float velocidadY = 5.0f;
    float rotacionX;
    float rotacionY;

    Camera camara;

    void Awake() {
        camara = transform.FindChild("Camera").GetComponent<Camera>();
        rotacionX = 0.0f;
        rotacionY = 0.0f;
    }

    public void capturaRaton() {
        rotacionX += velocidadX * Input.GetAxis("Mouse X");
        rotacionY -= velocidadY * Input.GetAxis("Mouse Y");

        if (GetComponent<PlayerManager>().estadoPlayer != PlayerManager.EstadosPlayer.Muriendo) {
            camara.transform.eulerAngles = new Vector3(rotacionY, rotacionX, 0.0f);
            transform.eulerAngles = new Vector3(0.0f, rotacionX + correccion, 0.0f);
        }
    }
}
