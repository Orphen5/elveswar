﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts;
using Assets.Scripts.Beans;
using System.Collections;
using UnityEngine.EventSystems;

public class NavigatorController : MonoBehaviour {

    public InputField userInput;
    public InputField passInput;
    public InputField emailInput;
    public InputField verifiPassInput;
    public SpriteRenderer errorPass;
    public SpriteRenderer errorMail;
    public SpriteRenderer userExist;
    private EventSystem system;

    void Start()
    {
        system = EventSystem.current;
    }

    public void Click(string direccion)
    {
        Debug.Log(direccion);
        switch (direccion) {
            /*  PANTALLA LOGIN  */
            case "submit":
                DAO.cleanLobby();
                userExist.enabled = false;
                errorPass.enabled = false;
                string nameUser = userInput.text;
                DAO.userDisconnected(nameUser);
                int opcion = DAO.getLogin(nameUser, passInput.text);
                Debug.Log(opcion);
                switch (opcion) { 
                    case 1:
                        GameManager.user = new UserBean();
                        GameManager.user.UserStats = DAO.getUserStats(nameUser);
                        Debug.Log(GameManager.user.UserStats.Kills);
                        Debug.Log("User " + GameManager.user.UserStats.Username + " actual char = " + GameManager.user.CharStats.Id);
                        DAO.userConnected(nameUser);
                        SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
                        break;
                    case 2:
                        userExist.enabled = true;
                        break;
                    case 3:

                        errorPass.enabled = true;
                        break;
                    default:
                        //enseñar mensaje servidor no operativo
                        break;
                }
                break;

            case "register":
              SceneManager.LoadScene("RegisterUi", LoadSceneMode.Single);
              break;


            /*  PANTALLA REGISTRO  */
            case "signIn":
                Debug.Log("in register done case");
                userExist.enabled = false;
                errorMail.enabled = false;
                errorPass.enabled = false;
                if (DAO.mailExists(emailInput.text) == 0 && Utility.validateEmail(emailInput.text))
                {

                    if (passInput.text.Equals(verifiPassInput.text))
                    {
                        if (DAO.userExists(userInput.text) == 0)
                        {

                            DAO.registerUser(userInput.text, verifiPassInput.text, emailInput.text);
                            passInput.text = "";
                            userInput.text = "";
                            emailInput.text = "";
                            verifiPassInput.text = "";                            
                            SceneManager.LoadScene("firstUi", LoadSceneMode.Single);
                        }
                        else
                        {
                            userExist.enabled = true;
                        }
                    }
                    else
                    {
                        errorPass.enabled = true;
                        Debug.Log("else in register case");
                    }
                }
                else {
                    errorMail.enabled = true;
                }
                
                break;

            case "goPlay":
                if (DAO.getLobbyUsers(GameManager.user.UserStats.Username))
                {
                    SceneManager.LoadScene("preparingGame", LoadSceneMode.Single);
                }
                
                /* no users actually */
                else 
                {
                    DAO.registerLobbly(GameManager.user.UserStats.Username, Utility.ipLocalGetter());
                    Debug.Log("User " + GameManager.user.UserStats.Username + " actual char = " + GameManager.user.CharStats.Id);
                    SceneManager.LoadScene("SelectCharUi", LoadSceneMode.Single);
                }
                break;

            /* PANTALLA  WORLD RANKING */
            case "worldRanking":
                SceneManager.LoadScene("worldRanking", LoadSceneMode.Single);
                
                break;

            /* CASO EXIT GENERICO */

            case "exit":
                Debug.Log("in exit case");
                //DAO.userDisconnected(GameManager.user.UserStats.Username);
                SceneManager.LoadScene("firstUi", LoadSceneMode.Single);
                break;

            case "exitApp":
                Application.Quit();
                break;

            case "exitFromReg":
                Debug.Log("in exit case");
                SceneManager.LoadScene("firstUi", LoadSceneMode.Single);
                break;

            case "goMenu":
                Debug.Log("in goMenu case");
                SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
                break;

            default:
                SceneManager.LoadScene("firstUi", LoadSceneMode.Single);
                break;

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Selectable next = system.currentSelectedGameObject.GetComponent<Selectable>().FindSelectableOnDown();

            if (next != null)
            {
                InputField inputfield = next.GetComponent<InputField>();
                if (inputfield != null)
                    inputfield.OnPointerClick(new PointerEventData(system));

                system.SetSelectedGameObject(next.gameObject, new BaseEventData(system));
            }
        }
    }
}