﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class preparingGame : MonoBehaviour {

    bool wait = true;

    void Start()
    {
        DAO.registerLobbly(GameManager.user.UserStats.Username, Utility.ipLocalGetter());
        StartCoroutine(chechLobby());
    }

    IEnumerator chechLobby()
    {
        while (wait)
        { 
            yield return new WaitForSeconds(1);
            wait = DAO.waitLobbyToSelectCharacter(GameManager.user.UserStats.Username);
            if (!wait)
            {
                player2toSelect();
            }
        }
    }

    void player2toSelect()
    {
        GameManager.user.CharRival = DAO.getCharSelected(GameManager.user.UserStats.Username);
        Debug.Log("Enviamos 2º player a select.");        
        SceneManager.LoadScene("SelectCharUi", LoadSceneMode.Single);
    }
}