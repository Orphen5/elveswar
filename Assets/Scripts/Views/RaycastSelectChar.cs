﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RaycastSelectChar : MonoBehaviour {
    public static string CharType;
    RaycastHit hit;
    Ray ray;

    private static bool charSelected = false;

    public void detectaClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 3000))
            {
                Debug.DrawLine(ray.origin, hit.point);
                Debug.Log(hit.transform.name);
                string nombreChar = hit.transform.name;
                if ("elfaNocheRuge".Equals(nombreChar) || "elfoSangreRuge".Equals(nombreChar))
                {
                    Debug.Log("nombre char: " + nombreChar);
                    switch (nombreChar)
                    {
                        case "elfaNocheRuge":
                            CharType = "A";
                            Debug.Log("elfa Noche: " + CharType);
                            //lanza animacion ruge          
                            break;

                        case "elfoSangreRuge":
                            CharType = "B";
                            Debug.Log("elfo Sangre: " + CharType);
                            //lanza animacion ruge     
                            break;
                    }                        
                    GameManager.user.CharStats.Id = CharType;
                    Debug.Log("User.CharStats.id: " + GameManager.user.CharStats.Id);
                    if (!CharType.Equals(GameManager.user.CharRival))
                    {
                        charSelected = true;
                    }
                }
            }
        }
    }

    void Update()
    {
        detectaClick();
        if (charSelected)
        {
            charSelected = false;
            persistChar();
        }

    }

    void persistChar()
    {
        DAO.registerCharInLobbly(GameManager.user.UserStats.Username, GameManager.user.CharStats.Id);
        GameManager.user.CharStats = DAO.getCharStats(GameManager.user.CharStats.Id);
        SceneManager.LoadScene("startingGame", LoadSceneMode.Single);
    }

    // Use this for initialization
    void Start () {
        Debug.Log("User " + GameManager.user.UserStats.Username + " is in char selection.");
        if (!"".Equals(GameManager.user.CharRival))
        {
            GameObject.FindGameObjectWithTag(GameManager.user.CharRival).transform.FindChild("CanvasSelected").gameObject.SetActive(true);
        }
    }
}