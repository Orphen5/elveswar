﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartingGame : MonoBehaviour {

    bool wait = true;
    bool FLAGstartGame = false;

    void Start()
    {
        StartCoroutine(startGame());
    }

    IEnumerator startGame()
    {
        while (wait)
        {
            yield return new WaitForSeconds(2);            
            FLAGstartGame = DAO.startGame();
            Debug.Log("Estado de partida: " + FLAGstartGame);
            if (FLAGstartGame)
            {
                wait = false;
                GameManager.user.IpRival = DAO.getIpVersus(GameManager.user.UserStats.Username);
                Debug.Log("Lanzo partida ip adversario: " + GameManager.user.IpRival);
                SceneManager.LoadScene("scene1", LoadSceneMode.Single);
            }
        }
    }
}