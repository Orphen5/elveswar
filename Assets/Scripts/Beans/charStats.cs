﻿
namespace Assets.Scripts.Beans 
{
    public class CharStats 
        {
        private string id;
        private int hp;
        private int damage;
        private int speed;
        private int mp;
        private int shieldP;

        public CharStats()
        {
            ini();
        }

        private void ini()
        {
            this.Id = "Z";
            this.Hp = 0;
            this.Damage = 0;
            this.Speed = 0;
            this.Mp = 0;
            this.ShieldP = 0;
        }

        public CharStats(string id, int hp, int damage, int speed, int mp, int shieldP) {
            this.Id = id;
            this.Hp = hp;
            this.Damage = damage;
            this.Speed = speed;
            this.Mp = mp;
            this.ShieldP = shieldP;
        }

        public string Id {
            get {
                return id;
            }

            set {
                id = value;
            }
        }

        public int Hp {
            get {
                return hp;
            }

            set {
                hp = value;
            }
        }

        public int Damage {
            get {
                return damage;
            }

            set {
                damage = value;
            }
        }

        public int Speed {
            get {
                return speed;
            }

            set {
                speed = value;
            }
        }

        public int Mp {
            get {
                return mp;
            }

            set {
                mp = value;
            }
        }

        public int ShieldP {
            get {
                return shieldP;
            }

            set {
                shieldP = value;
            }
        }
    }
}
