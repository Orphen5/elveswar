﻿
namespace Assets.Scripts.Beans 
{
    public class UserBean 
        {
        private CharStats charStats;
        private UserStats userStats;
        private string ipRival;
        private string charRival = "";

        public UserBean() {
            CharStats = new CharStats();
        }

        public void destroyChar() {
            charStats = new CharStats();
        }

        public UserStats UserStats {
            get {
                return userStats;
            }

            set {
                userStats = value;
            }
        }

        public CharStats CharStats {
            get {
                return charStats;
            }

            set {
                charStats = value;
            }
        }

        public string IpRival
        {
            get
            {
                return ipRival;
            }

            set
            {
                ipRival = value;
            }
        }

        public string CharRival
        {
            get
            {
                return charRival;
            }

            set
            {
                charRival = value;
            }
        }
    }
}
