﻿
namespace Assets.Scripts
{
    public class UserStats
    {
        private string username;
        private int kills;
        private int deaths;
        private int balance;
        private int hits;
        private int shots;
        private string accuracy;
        private int wins;
        private int loses;

        public UserStats(string username)
        {
            this.Username = username;
            ini();
        }

        private void ini()
        {
            this.Kills = 0;
            this.Deaths = 0;
            this.Hits = 0;
            this.Shots = 0;
            this.Wins = 0;
            this.Loses = 0;
            this.Balance = 0;
            this.Accuracy = "";

        }

        public UserStats(string username, int kills, int deaths, int hits, int shots, int wins, int loses)
        {
            this.Username = username;
            this.Kills = kills;
            this.Deaths = deaths;
            this.Hits = hits;
            this.Shots = shots;
            this.Wins = wins;
            this.Loses = loses;
            this.Balance = kills - deaths;
            string pAccuracy = "" + ((double)(hits / shots) * 100);
            pAccuracy.Substring(0, 5);
            pAccuracy += "%";
            this.Accuracy = pAccuracy;
        }
        public void recalcularStats()
        {
            string pAccuracy = "" + ((double)(hits / shots) * 100);
            pAccuracy.Substring(0, 5);
            pAccuracy += "%";
            this.Accuracy = pAccuracy;
            balance = kills - deaths;
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public int Kills
        {
            get
            {
                return kills;
            }

            set
            {
                kills = value;
            }
        }

        public int Deaths
        {
            get
            {
                return deaths;
            }

            set
            {
                deaths = value;
            }
        }

        public int Balance
        {
            get
            {
                return balance;
            }

            set
            {
                balance = value;
            }
        }

        public int Hits
        {
            get
            {
                return hits;
            }

            set
            {
                hits = value;
            }
        }

        public int Shots
        {
            get
            {
                return shots;
            }

            set
            {
                shots = value;
            }
        }

        public string Accuracy
        {
            get
            {
                return accuracy;
            }

            set
            {
                accuracy = value;
            }
        }

        public int Wins
        {
            get
            {
                return wins;
            }

            set
            {
                wins = value;
            }
        }

        public int Loses
        {
            get
            {
                return loses;
            }

            set
            {
                loses = value;
            }
        } 
    }
}
