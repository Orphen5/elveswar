﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class InstanciaPlayer : NetworkBehaviour {

    public static InstanciaPlayer instance;
    GameObject playerInstance;

    void Awake() {
        instance = this;
    }

    [Command]
    public void CmdInstanciarPlayer(Vector3 posInstancia) {
        string nombreModelo = "elfoSangreHunter";

        //Asociamos el prefab con el gameObject
        GameObject player = (GameObject)Resources.Load("Models\\Characters\\Prefabs\\" + nombreModelo, typeof(GameObject));

        //Creamos una instancia de player
        playerInstance = (Instantiate(player, posInstancia, Quaternion.identity) as GameObject);
        NetworkServer.Spawn(playerInstance);
    }
}
