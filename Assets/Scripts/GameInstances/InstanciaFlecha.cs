﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class InstanciaFlecha : NetworkBehaviour {

    GameObject arrow;
    GameObject arrowInstance;

    void Awake() {
        arrow = (GameObject)Resources.Load("Models\\Characters\\Prefabs\\arrow", typeof(GameObject));
    }

    [Command]
    public void CmdInstanciarFlecha() {
        Vector3 posPuntero = transform.FindChild("Camera").transform.FindChild("Puntero").transform.position;
        Transform camera = transform.FindChild("Camera").transform;
        arrowInstance = Instantiate(arrow, new Vector3(posPuntero.x, posPuntero.y, posPuntero.z), Quaternion.Euler(0, camera.eulerAngles.y + 90, camera.eulerAngles.x));
        changeLayer(arrowInstance);
        NetworkServer.Spawn(arrowInstance);
        RpcAddForceOnAll(arrowInstance);
        GetComponent<PlayerManager>().disparos++;
    }

    [ClientRpc]
    void RpcAddForceOnAll(GameObject bullet) {
        bullet.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(-1, 0, 0) * GetComponent<PlayerStats>().fuerzaDisparo, ForceMode.Impulse);
    }

    void changeLayer(GameObject arrow) {
        arrow.layer = LayerMask.NameToLayer(gameObject.tag);
    }
}
